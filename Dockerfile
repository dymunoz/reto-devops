FROM node:14

RUN mkdir -p /home/node/app_node && chown -R node:node /home/node/app_node
WORKDIR /home/node/app_node
COPY . /home/node/app_node
USER node
RUN npm install
RUN npm run test
CMD [ "node", "index.js" ]